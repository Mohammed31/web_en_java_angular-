package com.benali.projetdefin.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;


@Entity // cela signifie quelle doit persisster dans la base de donner 
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@SuppressWarnings("serial")
public class Module implements Serializable {
	
	
	@Id // signifie que cette attribut est l id de lentiter  
	@GeneratedValue(strategy=GenerationType.IDENTITY) // pour que le id soit generer automatiqument 
	private int id;
	private String nomModule;
	
	@OneToMany(mappedBy="module",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private Collection<Etudiant> etudiants;
	
	@JsonIgnore
	private Professeur professeur;
	
	
	public Module(int id, String nomModule) {
		super();
		this.id = id;
		this.nomModule = nomModule;
	}

	public Module(String nomModule) {
		super();
		this.nomModule = nomModule;
	}
	
	

	public Module() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomModule() {
		return nomModule;
	}

	public void setNomModule(String nomModule) {
		this.nomModule = nomModule;
	}

	@JsonIgnore
	public Collection<Etudiant> getEtudiant() {
		return etudiants;
	}
	@JsonSetter
	public void setEtudiant(Collection<Etudiant> etudiant) {
		this.etudiants = etudiant;
	}
	@JsonIgnore
	public Professeur getProfesseur() {
		return professeur;
	}
	@JsonSetter
	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}
	
	
	
	

}

package com.benali.projetdefin.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity // cela signifie quelle doit persisster dans la base de donner
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//@SuppressWarnings("serial")
public class Etudiant implements Serializable {
	
	@Id // signifie que cette attribut est l id de lentiter  
	@GeneratedValue(strategy=GenerationType.IDENTITY) // pour que le id soit generer automatiqument 
	private int id;
	private String nom;
	private String prenom;
	private String adresse;
	private String tel;
	private String mail;
	
	// les relation entre les tables 
	@ManyToOne  // la relation entre la table etudiant et prof
	@JoinColumn(name="code_professeur")
	private Professeur professeur;
	@ManyToOne // la relation entre la table etudiant et module  
	private Module module;
	
	// les constructeurs 
	public Etudiant() {
		super();
	}
	public Etudiant(int id, String nom, String prenom, String adresse, String tel, String mail) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.tel = tel;
		this.mail = mail;
	}
	public Etudiant(String nom, String prenom, String adresse, String tel, String mail) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.tel = tel;
		this.mail = mail;
	}
	
	// les getters et setters 
	public int getId() {
		return id;
	}
	@JsonSetter
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	
	@JsonIgnore
	public Professeur getProfesseur() {
		return professeur;
	}
	@JsonSetter
	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}
	@JsonIgnore
	public Module getModule() {
		return module;
	}
	@JsonIgnore
	public void setModule(Module module) {
		this.module = module;
	}
	// le to string 
	@Override
	public String toString() {
		return "Etudiant [nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", tel=" + tel + ", mail="
				+ mail + "]";
	}
	
}

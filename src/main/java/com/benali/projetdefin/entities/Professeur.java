package com.benali.projetdefin.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;



@Entity // cela signifie quelle doit persisster dans la base de donner 
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@SuppressWarnings("serial")
public class Professeur implements Serializable {
	
	@Id // signifie que cette attribut est l id de lentiter  
	@GeneratedValue(strategy=GenerationType.IDENTITY) // pour que le id soit generer automatiqument 
	private int id;
	private String nom;
	private String prenom;
	private String adresse;
	private String tel;
	private String mail;
	
	
	@OneToOne
	@JsonIgnore
	private Module module;
	
	@OneToMany(mappedBy="professeur",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	@JsonIgnore
	private Collection<Etudiant> etudiant;

	
	@JsonIgnore
	public Module getModule() {
		return module;
	}
	@JsonSetter
	public void setModule(Module module) {
		this.module = module;
	}
	
	
	// les getters et setters 
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		
		@JsonIgnore
		public Collection<Etudiant> getEtudiant() {
			return etudiant;
		}
		@JsonSetter
		public void setEtudiant(Collection<Etudiant> etudiant) {
			this.etudiant = etudiant;
		}

		// le to string 
		@Override
		public String toString() {
			return "Etudiant [nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", tel=" + tel + ", mail="
					+ mail + "]";
		}
	

}

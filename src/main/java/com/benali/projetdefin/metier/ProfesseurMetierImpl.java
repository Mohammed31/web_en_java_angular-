package com.benali.projetdefin.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.benali.projetdefin.dao.IProfesseurRepository;
import com.benali.projetdefin.entities.Professeur;

@Service
public class ProfesseurMetierImpl implements ProfesseurMetier {

	@Autowired
	private IProfesseurRepository professeurRepository;
	
	
	@Override
	public Professeur saveProfesseur( Professeur prof) {
		
		return professeurRepository.save(prof);
	}

	@Override
	public void updateProfesseur( Professeur prof) {
		 professeurRepository.saveAndFlush(prof);
	}

	@Override
	public void deleteProfesseur(int id) {
		
		professeurRepository.deleteById(id);
		
	}

	@Override
	public Professeur getProfesseur(int id) {
		// TODO Auto-generated method stub
		return professeurRepository.getOne(id);
	}

	@Override
	public List<Professeur> getListProfesseurs() {
		
		return professeurRepository.findAll();
	}

	public IProfesseurRepository getProfesseurRepository() {
		return professeurRepository;
	}

	public void setProfesseurRepository(IProfesseurRepository professeurRepository) {
		this.professeurRepository = professeurRepository;
	}

}

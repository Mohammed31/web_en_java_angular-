package com.benali.projetdefin.metier;

import java.util.List;

import com.benali.projetdefin.entities.Etudiant;
import com.benali.projetdefin.entities.Professeur;

public interface ProfesseurMetier {
	
	public Professeur saveProfesseur(Professeur prof);
	public void updateProfesseur(Professeur prof);
	public void deleteProfesseur(int id);
	public Professeur getProfesseur(int id);
	public List<Professeur> getListProfesseurs();

}

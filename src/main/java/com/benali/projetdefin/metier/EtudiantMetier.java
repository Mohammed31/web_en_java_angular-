package com.benali.projetdefin.metier;

import java.util.List;

import com.benali.projetdefin.entities.Etudiant;

public interface EtudiantMetier {
	
	public Etudiant saveEtudiant(Etudiant etud);
	public Etudiant updateEtudiant(Etudiant etud);
	public void deleteEtudiant(int id);
	public Etudiant getEtudiant(int id);
	public List<Etudiant> getListEtudiants();
	

}

package com.benali.projetdefin.metier;

import java.util.List;

import com.benali.projetdefin.entities.Etudiant;
import com.benali.projetdefin.entities.Module;

public interface ModuleMetier {
	
	public Module saveModule(Module modul);
	public void updateModule(Module modul);
	public void deleteModule(int id);
	public Module getModule(int id);
	public List<Module> getListModules();

}

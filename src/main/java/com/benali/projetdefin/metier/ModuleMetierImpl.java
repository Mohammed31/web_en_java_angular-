package com.benali.projetdefin.metier;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.benali.projetdefin.dao.IModuleRepository;
import com.benali.projetdefin.entities.Module;

@Service
public class ModuleMetierImpl implements ModuleMetier {

	@Autowired
	private IModuleRepository moduleRepository;
	
	
	@Override
	public Module saveModule( Module modul) {
		
		return moduleRepository.save(modul);
	}

	@Override
	public void updateModule( Module modul) {
		
		 moduleRepository.saveAndFlush(modul);
	}

	@Override
	public void deleteModule( int id) {
		moduleRepository.deleteById(id);;
		
	}

	@Override
	public Module getModule(int id) {
		
		return moduleRepository.getOne(id);
	}

	@Override
	public List<Module> getListModules() {
		
		return moduleRepository.findAll();
	}

	public IModuleRepository getModuleRepository() {
		return moduleRepository;
	}

	public void setModuleRepository(IModuleRepository moduleRepository) {
		this.moduleRepository = moduleRepository;
	}

}

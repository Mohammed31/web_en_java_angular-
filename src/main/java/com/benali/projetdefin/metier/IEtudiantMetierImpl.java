package com.benali.projetdefin.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.benali.projetdefin.dao.IEtudiantRepository;
import com.benali.projetdefin.entities.Etudiant;

@Service
public class IEtudiantMetierImpl implements EtudiantMetier {
	
	@Autowired
	private IEtudiantRepository etudiantRepository;
	
	@Override
	public Etudiant saveEtudiant( Etudiant etud) {
		// TODO Auto-generated method stub
		return etudiantRepository.save(etud);
	}

	@Override
	public Etudiant updateEtudiant( Etudiant etud) {
		// TODO Auto-generated method stub
		return etudiantRepository.saveAndFlush(etud);
	}

	@Override
	public void deleteEtudiant(int id) {
		etudiantRepository.deleteById(id);
		
	}

	@Override
	public Etudiant getEtudiant(int id) {
		// TODO Auto-generated method stub
		return etudiantRepository.getOne(id);
	}

	@Override
	public List<Etudiant> getListEtudiants() {
		// TODO Auto-generated method stub
		return etudiantRepository.findAll();
	}

	public IEtudiantRepository getEtudiantRepository() {
		return etudiantRepository;
	}

	public void setEtudiantRepository(IEtudiantRepository etudiantRepository) {
		this.etudiantRepository = etudiantRepository;
	}
	

}

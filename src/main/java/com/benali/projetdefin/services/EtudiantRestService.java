package com.benali.projetdefin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.benali.projetdefin.entities.Etudiant;
import com.benali.projetdefin.metier.EtudiantMetier;

// pour indiquer que cest un service web rest 
@RestController
// pour autoriser les requettes venu du service front 
// generalement le navigateur refuse tout requette par securité 
@CrossOrigin(origins = {"*"})

// pour eviter a recrire a chaque methode la  (value="/etudiants"
@RequestMapping(value="/etudiants")
public class EtudiantRestService {
	@Autowired
	private EtudiantMetier etudiantMetier;

	// la methide pour enregistrer un etudiant 
	@RequestMapping(value ="/save", method = RequestMethod.POST, consumes="application/json" )
	public Etudiant saveEtudiant(@RequestBody Etudiant etud) {
		return etudiantMetier.saveEtudiant(etud);
	}

	// la methide pour mettre a jour un etudiant et au fond de traitement cest la meme chose 
	// que de rajouter un etudiant 
	@RequestMapping(value ="/update", method = RequestMethod.POST,consumes="application/json")
	public Etudiant updateEtudiant(@RequestBody Etudiant etud) {
		return etudiantMetier.updateEtudiant(etud);
	}
 
	// la methide pour supprimer un etudiant 
	// il ya deux possibiliter 
	// passer un objet etudiant ou ID 
	// je previlege par id 
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteEtudiant(@PathVariable("id") int id) {
		etudiantMetier.deleteEtudiant(id);
	}
	
	// la meme procedure que pour supprimer  // la je veux trouver un etudiant 
	// il ya deux possibiliter 
		// passer un objet etudiant ou ID 
		// je previlege par id 
	@RequestMapping(value = "find/{id}", method = RequestMethod.GET)
	public Etudiant getEtudiant(@PathVariable("id")  int id) {
		return etudiantMetier.getEtudiant(id);
	}

	// c'est une methode qui utilise " method = RequestMethod.GET " pour recuprer des donner de la base de donner 
	// elle ne fait aucune action sur la base de donner 
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Etudiant> getListEtudiants() {
		return etudiantMetier.getListEtudiants();
	}

}

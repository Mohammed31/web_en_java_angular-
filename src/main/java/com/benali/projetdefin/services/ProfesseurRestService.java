package com.benali.projetdefin.services;

import java.util.List;

import org.apache.tomcat.util.http.parser.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.benali.projetdefin.entities.Professeur;
import com.benali.projetdefin.metier.ProfesseurMetier;


@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping(value="/professeurs")
public class ProfesseurRestService {
	
	@Autowired
	private ProfesseurMetier professeurMetier;

	//@RequestMapping(method=RequestMethod.POST, value="/save", consumes="application/json", produces="application/json")
	@RequestMapping(value="/save",method=RequestMethod.POST,consumes="application/json")
	public Professeur saveProfesseur(@RequestBody Professeur prof) {
		return professeurMetier.saveProfesseur(prof);
	}
	@RequestMapping(value="/update",method=RequestMethod.POST,consumes="application/json")
	public void updateProfesseur(@RequestBody Professeur prof) {
		 professeurMetier.updateProfesseur(prof);
	}

	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public void deleteProfesseur(@PathVariable("id") int id) {
		professeurMetier.deleteProfesseur(id);
	}

	@RequestMapping(value="find/{id}",method=RequestMethod.GET)
	public Professeur getProfesseur(@PathVariable("id")  int id) {
		return professeurMetier.getProfesseur(id);
	}

	@RequestMapping(value="/list",method=RequestMethod.GET)
	public List<Professeur> getListProfesseurs() {
		return professeurMetier.getListProfesseurs();
	}

}

package com.benali.projetdefin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.benali.projetdefin.entities.Module;
import com.benali.projetdefin.metier.ModuleMetier;

@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping(value="/modules")
public class ModuleRestService {

	@Autowired
	private ModuleMetier moduleMetier;

	@RequestMapping(value = "/save", method = RequestMethod.POST,consumes="application/json")
	public Module saveModule(@RequestBody Module modul) {
		return moduleMetier.saveModule(modul);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST,consumes="application/json")
	public void updateModule(@RequestBody Module modul) {
		 moduleMetier.updateModule(modul);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteModule(@PathVariable("id") int id) {
		moduleMetier.deleteModule(id);
	}

	@RequestMapping(value = "find/{id}", method = RequestMethod.GET)
	public Module getModule(@PathVariable ("id") int id) {
		return moduleMetier.getModule(id);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Module> getListModules() {
		return moduleMetier.getListModules();
	}

}

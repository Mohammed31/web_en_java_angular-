package com.benali.projetdefin.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.benali.projetdefin.entities.Professeur;

public interface IProfesseurRepository extends JpaRepository<Professeur, Integer> {

}

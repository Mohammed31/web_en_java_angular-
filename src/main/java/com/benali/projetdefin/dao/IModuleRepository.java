package com.benali.projetdefin.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.benali.projetdefin.entities.Module;

public interface IModuleRepository extends JpaRepository<Module, Integer> {

}

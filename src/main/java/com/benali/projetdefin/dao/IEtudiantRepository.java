package com.benali.projetdefin.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.benali.projetdefin.entities.Etudiant;

public interface IEtudiantRepository extends JpaRepository<Etudiant, Integer > {

}

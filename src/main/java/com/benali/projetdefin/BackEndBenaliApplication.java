package com.benali.projetdefin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndBenaliApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndBenaliApplication.class, args);
	}
}
